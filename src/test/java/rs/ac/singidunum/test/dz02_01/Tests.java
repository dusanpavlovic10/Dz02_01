package rs.ac.singidunum.test.dz02_01;

import org.junit.Assert;
import org.junit.Test;
import rs.ac.singidunum.dz02_01.HelloWorldImpl;
import rs.ac.singidunum.dz02_01.IHelloWorld;

public class Tests {

	private final IHelloWorld helloWorld = new HelloWorldImpl();

	@Test
	public void testHelloWorld() {
		Assert.assertEquals("Hello World", this.helloWorld.helloWorld());
	}
}
